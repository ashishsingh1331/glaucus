# [Glaucus Task]

Steps

1) Clone the git repo - `git clone https://calipus@bitbucket.org/calipus/glaucus.git && cd glaucus` 
 
2) Run npm start
 
3) If CORS issue occurs use following browser extension
    a) For chrome [CORS Toggle](https://chrome.google.com/webstore/detail/cors-toggle/omcncfnpmcabckcddookmnajignpffnh?hl=en)
    b) For firefox [CORS everywhere](https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere/)