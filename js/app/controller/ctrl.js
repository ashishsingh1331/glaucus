app.value('urlBase','https://devbackend.gscmaven.com:8443/omsservices/webapi/systemconfig');

app.controller('frontController', ['$scope', 'configs', function ($scope, configs) {


    $scope.gridOptions = {
        enableSorting: true,
        columnDefs: [
            {name: 'System Config Id', field: 'idtableClientSystemConfigId'},
            {name: 'System config name', field: 'tableClientSystemConfigConfigName'},
            {name: 'System config value', field: 'tableClientSystemConfigValue', enableCellEdit: true, }
        ],
        data: configs
    };


    $scope.gridOptions.onRegisterApi = function (gridApi) {
        //set gridApi on scope
        $scope.gridApi = gridApi;
        gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
            rowEntity.$update();
            $scope.$apply();
        });
    };


}]);



