/**
 * Created by ashish on 18/3/17.
 */
var app = angular.module('app', [
    'ui.router',
    'angular-loading-bar',
    'ngResource',
    'ui.grid',
    'ngTouch',
    'ui.grid.edit'
]);

app.config(function($stateProvider, $urlRouterProvider) {


    // For any unmatched url, redirect to /
    $urlRouterProvider.otherwise("/");


    var front = {
        name:'front',
        url:'/',
        templateUrl: "js/app/templates/albums.html",
        controller:'frontController',
        resolve: {

            configs: function(api) {
              return  api.query().$promise.then(function(data){
                    return data;
                });
            },

        },
    };

    $stateProvider
        .state(front);

});