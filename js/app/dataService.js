/**
 * Created by ashish on 13/1/16.
 */
app.factory('api', function ($resource,urlBase) {

    return $resource(urlBase + '/:id', {id: '@idtableClientSystemConfigId'}, {
        update: {
            method: 'PUT' // this method issues a PUT request
        }
    });
});




